
function floatValidator(float){
    return float.toPrecision(5);
}


Blockly.Blocks['program'] = {
    init: function() {
        this.appendDummyInput()
            .appendField("begin");
        this.appendStatementInput("ME")
            .setCheck("MeasurementElement");
        this.appendDummyInput()
            .appendField("end");
        this.setInputsInline(false);
        this.setOutput(true, "Program");
        this.setColour(345);
        this.setTooltip("");
        this.setHelpUrl("");
    }
  };



Blockly.Python['program'] = function(block) {
    var define_blocks =  block.getInputTargetBlock('ME');
    var code = 'Prog [' ;
    if (define_blocks){
        code += Blockly.Python.blockToCode(define_blocks,true)
        while (define_blocks = define_blocks.getNextBlock()) { 
            code += ', ' + Blockly.Python.blockToCode(define_blocks,true);   
        }
    }
    // TODO: Assemble JavaScript into code variable.
    code +=  ']';
    // TODO: Change ORDER_NONE to the correct strength.
    return [code, Blockly.JavaScript.ORDER_MEMBER ];
};


/**
 * MEWait block definition
 */
Blockly.Blocks['me_wait'] = {
    init: function() {
        var time = new Blockly.FieldNumber(0, 0);
        this.appendDummyInput()
            .appendField("Wait")
            .appendField(time, "time")
            .appendField("s");
        this.setPreviousStatement(true, "MeasurementElement");
        this.setNextStatement(true, "MeasurementElement");
        this.setColour(30);
        this.setTooltip("");
        this.setHelpUrl("");
        time.setValidator(floatValidator);
    }
  };


Blockly.Python['me_wait'] = function(block) {
    var number_value = block.getFieldValue('time');
    // TODO: Assemble JavaScript into code variable.
    var code = 'MEWait ' + number_value;
    return code
};






Blockly.Blocks['me_timelapse'] = {
    init: function() {
        var duration = new Blockly.FieldNumber(0, 0);
        this.appendDummyInput()
            .appendField("Timelapse:");
        this.appendDummyInput()
            .appendField("Duration:")
            .appendField(new Blockly.FieldNumber(0, 0), "Duration");
        this.appendDummyInput()
            .appendField("Interval")
            .appendField(duration, "Interval");
        this.appendValueInput("Prog")
            .setCheck("Program")
            .appendField("Program");
        this.setPreviousStatement(true, "MeasurementElement");
        this.setNextStatement(true, "MeasurementElement");
        this.setColour(30);
        this.setTooltip("tooltip");
        this.setHelpUrl("");
        var validator = function(newValue) {
            console.log(newValue)
            return newValue;
        };
      
        duration.setValidator(floatValidator);
    }
  };

Blockly.Python['me_timelapse'] = function(block) {
    var number_duration = block.getFieldValue('Duration');
    var number_interval = block.getFieldValue('Interval');
    var value_prog = Blockly.Python.valueToCode(block, 'Prog', Blockly.Python.ORDER_ATOMIC);
    // TODO: Assemble JavaScript into code variable.
    var code = 'METimeLapse ' + number_duration + ' ' + number_interval + ' ' + value_prog  ;
    return code;
};


Blockly.Blocks['me_do_times'] = {
    init: function() {
        var times = new Blockly.FieldNumber(1, 0, Number.MAX_SAFE_INTEGER, 1);
        this.appendDummyInput()
            .appendField("Do Times:");
        this.appendDummyInput()
            .appendField("Times:")
            .appendField(times, "Times");
        this.appendValueInput("Prog")
            .setCheck("Program")
            .appendField("Program");
        this.setPreviousStatement(true, "MeasurementElement");
        this.setNextStatement(true, "MeasurementElement");
        this.setColour(100);
        this.setTooltip("tooltip");
        this.setHelpUrl("");
        var validator = function(newValue) {
            console.log(newValue)
            return newValue;
        };
      
        times.setValidator(validator);
    }
};

Blockly.Python['me_do_times'] = function(block){
    var number_times = block.getFieldValue('Times');
    var value_prog = Blockly.Python.valueToCode(block, 'Prog', Blockly.Python.ORDER_ATOMIC);
    var code = 'MEDoTimes ' + number_times + ' ' + value_prog;
    return code;
};