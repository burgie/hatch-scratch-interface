'use strict'

let workspace


Promise.all([
    $.getJSON( "static/block_def.json"),
    $.getJSON( "static/toolbox.json"),
]).then(([blockDef,toolbox]) => {
    console.log([blockDef,toolbox])

    Blockly.defineBlocksWithJsonArray(blockDef);
    workspace = Blockly.inject('blocklyDiv', {toolbox: toolbox});
    
    //Blockly.Xml.domToWorkspace(document.getElementById('startBlocks'), workspace);
}).catch(err => console.log(err));

$("#run_code").click(()=> {
    let code = Blockly.Python.workspaceToCode(workspace);
    new Promise(function(resolve,reject) {
        $.ajax({
            url: "/runCode",
            method:'POST',
            data: JSON.stringify({value:code}),
            success: data => resolve(data),
            error: xhr => reject(xhr),
            headers: {
                accept: "application/json",
                "Access-Control-Allow-Origin":"*",
                "Content-type":"application/json"
            }
        });
    }).then(data => {
        $("#result_tb").val(data);
    }).catch(err => {
        $("#result_tb").val("Query failed: " + err);
        console.log(err);
    });
});

