{-# LANGUAGE OverloadedStrings #-}
module Hatch (
    startApp,
    SiteConfig(..),
    module Types,
    module BlockToFile
) where


import Prelude hiding (read)


import BlockToFile
import Types
import Control.Monad
import Control.Applicative
import Snap.Core
import Snap.Http.Server
import Snap.Util.FileServe
import Data.Aeson as JSON
import Data.Maybe(fromJust)
import Control.Monad.IO.Class
import Data.Text (pack,unpack)
import Data.Maybe

type ReadFun a = (String -> Maybe [a])
type ExecFun a = ([a] -> IO())


data SiteConfig a = SiteConfig {read ::ReadFun a, exec:: ExecFun a, generateFiles::Bool}

startApp :: (Block a) => [Category] -> [a] -> SiteConfig a -> IO ()
startApp cs init config =
    do
        when (generateFiles config) (createFiles cs)
        quickHttpServe (site config)



site :: (Block a) => SiteConfig a ->  Snap ()
site x =
    ifTop (serveFile "web/index.html") <|>
    route [("runCode", runCode x )
       ,   ("/static",   serveDirectory "web/static")
       ,   ("/scripts",   serveDirectory "web/scripts")
     ]

runCode :: (Block a) => SiteConfig a -> Snap ()
runCode config = method POST $ do
    code <- readRequestBody 1000000 >>= return . fromJust . JSON.decode :: Snap Code
    case read config (value  code) of
        Just x ->
            do
                liftIO $ exec config x
                writeText $ pack $ show x
        _ ->
            do
                writeText "Error"
