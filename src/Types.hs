{-# LANGUAGE DeriveGeneric     #-}


module Types (
    Argument(..),
    InputType(..),
    HatchBlock(..),
    Category(..),
    Block(..),
    Code(..),
    generateCategories) where

    
import GHC.Generics
import Data.Aeson hiding (json)



data InputType = Int | Float | Str | Bool

type Prefix = String
type Suffix = String
type Colour = Int

type Name = String

data Argument = BasicArg InputType Name Prefix Suffix  -- Name  Suffix
              | TupleArg Name Prefix Suffix Argument Argument -- 
              | StatementArg Name Prefix String 
              | InputStatementArg String String
              | DummyArg

class (Read a,Show a ) => Block a where
    toBlock :: a -> HatchBlock 
    toBlocks :: [a] -> HatchBlock    


data Category = Category Name Colour [HatchBlock]


generateCategories :: (Block a) => String -> Colour -> [a] -> [Category]
generateCategories n c xs = [Category n c (toBlocks xs:map toBlock xs)] 


data HatchBlock
            = RegularBlock Name [Argument] -- Name [Arguments] 
            | ListBlock String String      -- TypeIn TypeOut 
            | BasicBlock Name Argument


newtype Code = Code 
    {
        value:: String
    } deriving (Generic, Show)


instance ToJSON Code

instance FromJSON Code



