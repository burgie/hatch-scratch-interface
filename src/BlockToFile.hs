{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE ExistentialQuantification #-}
{-# LANGUAGE AllowAmbiguousTypes#-}
module BlockToFile (
    createFiles
) where
 
import Prelude hiding (concat)
import Data.Aeson hiding (Bool)
import GHC.Exts   (fromList)
import qualified Data.ByteString.Lazy as B hiding (concatMap,foldr,map)
import System.Directory
import Data.Word8
import qualified Data.Vector as V
import Data.Maybe
import Text.Read
import Text.Parsec
import Text.Parsec.String
import Text.Parsec.Expr
import Text.ParserCombinators.Parsec.Number
import Text.Parsec.Language
import Text.ParserCombinators.Parsec.Error
import Types

    



createFiles :: [Category] -> IO()
createFiles cs = 
    do
        blocksToJson cs
        blocksToJS cs
        blocksToToolBox cs 


blocksToToolBox :: [Category] -> IO()
blocksToToolBox xs = B.writeFile "web/static/toolbox.json" (encode $ categoriesToJsonToolbox xs)

-- def json Toolbox: https://developers.google.com/blockly/guides/configure/web/toolbox
blockToJsonToolbox :: HatchBlock -> Value
blockToJsonToolbox x  = object ["kind" .= ("block"::String), "type" .= blockName x]

-- possible use when you don't want categories
blocksToJsonToolBox :: [HatchBlock] -> Value
blocksToJsonToolBox xs = object ["kind" .= ("flyoutToolbox"::String), "contents" .= map blockToJsonToolbox xs ]

categoriesToJsonToolbox :: [Category] -> Value
categoriesToJsonToolbox [Category _ _ xs] =  blocksToJsonToolBox xs
categoriesToJsonToolbox xs  = object ["kind" .= ("categoryToolbox"::String), "contents" .= map categoryToJsonToolbox xs ]

categoryToJsonToolbox :: Category -> Value
categoryToJsonToolbox (Category n _ xs) = object ["kind" .= ("category"::String), "name" .= n, "contents" .= map blockToJsonToolbox xs ]

blocksToJson:: [Category] -> IO()
blocksToJson xs =
    do
        createDirectoryIfMissing True  "web/static/"
        B.writeFile  "web/static/block_def.json"  (encode  (concatMap (\c -> map (\b-> hatchBlockToJson b c) (catBlocks c)) xs))



blocksToJS:: [Category] -> IO()
blocksToJS xs =
    do
        writeFile "web/static/block_def.js" ( concatMap (snd . toJS . catBlocks) xs)


instance ToJSON Argument where
    toJSON (BasicArg Int name  _ _)         = object ["name" .= name, "type" .= ("field_number"::String), "value".= (0::Int)]
    toJSON (BasicArg Float name  _ _)       = object ["name" .= name, "type" .= ("field_number"::String), "value".= (0::Int),"precision".= (0.001::Float)]
    toJSON (BasicArg Str name  _ _)         = object ["name" .= name, "type" .= ("field_input"::String) ,  "text".= name]
    toJSON (BasicArg Bool name  _ _)        = object ["name" .= name, "type" .= ("field_dropdown"::String) ,  "options" .= booleanOptions]
    toJSON (StatementArg name _ check)      = object ["name" .= name, "type" .= ("input_value"::String), "check" .= check ]
    toJSON (InputStatementArg name check)   = object ["name" .= name, "type" .= ("input_statement"::String), "check" .= check ]
    toJSON DummyArg                         = object ["type" .= ("input_dummy"::String)]

catName :: Category -> String
catName (Category n _ _) = n
catCol :: Category -> Int
catCol (Category _ c _ ) = c
catBlocks :: Category -> [HatchBlock]
catBlocks (Category _ _ xs) = xs

blockName :: HatchBlock ->  String
blockName (RegularBlock name _ ) = name
blockName (ListBlock _ name)     = name
blockName (BasicBlock name _)    = name


booleanOptions:: Value
booleanOptions = fromJust $ decode "[[\"True\",\"True\"],[\"False\",\"False\"]]"


instance ToJSON Category where    
    toJSON c      = Array (V.fromList $ map (`hatchBlockToJson` c) (catBlocks c)) 
    toJSONList cs = Array (V.fromList $ concatMap (\c->map (`hatchBlockToJson` c) (catBlocks c) ) cs)


hatchBlockToJson :: HatchBlock -> Category -> Value
hatchBlockToJson (RegularBlock name (a:args))  c   =  object [ "type" .= name,
                                                        "message0".= toMessage (a:concatMap generateMessageArg args),
                                                        "args0" .= (a:concatMap createArgument args),
                                                        "previousStatement".=  catName c,
                                                        "nextStatement".= catName c,
                                                        "inputsInline" .= False,
                                                        "colour".= catCol c,
                                                        "tooltip".= (""::String),
                                                        "helpUrl".= (""::String)
                                                        ]
hatchBlockToJson (ListBlock  typeIn typeOut) c     =  object [ "type" .= typeOut,
                                                        "message0" .= ("begin %1 %2 end"::String),
                                                        "args0" .= concatMap createArgument [InputStatementArg typeIn typeIn], --TODO: Statement argument mss herzien hier met de 2 typeIn die hetzelfde zijn
                                                        "output".= typeOut,
                                                        "colour".= catCol c,
                                                        "tooltip".= (""::String),
                                                        "helpUrl".= (""::String)
                                                        ]
hatchBlockToJson (BasicBlock name  a)        c     = object [ "type" .= name,
                                                        "message0".= toMessage [a],
                                                        "args0" .= createTupleArg a,
                                                        "previousStatement".=  catName c,
                                                        "nextStatement".= catName c,
                                                        "inputsInline" .= False,
                                                        "colour".= catCol c,
                                                        "tooltip".= (""::String),
                                                        "helpUrl".= (""::String)
                                                        ]


createArgument :: Argument -> [Argument]
createArgument DummyArg                = [DummyArg]
createArgument (TupleArg name p s x y) = createTupleArg (TupleArg name p s x y)
createArgument (InputStatementArg x y) = [DummyArg, InputStatementArg x y]
createArgument x                       = [DummyArg,x]

createTupleArg :: Argument -> [Argument]
createTupleArg (BasicArg t n p s)   = [BasicArg t n p s]
createTupleArg (TupleArg _ _ _ x y) = createTupleArg x ++ createTupleArg y
createTupleArg _                  = []


generateMessageArg :: Argument -> [Argument]
generateMessageArg DummyArg           = [DummyArg]
generateMessageArg (TupleArg name p s x y) = [TupleArg name p s x y]
generateMessageArg x                  = [DummyArg,x]

toMessage :: [Argument] -> String
toMessage = toMessage' 1

createIndex :: Int -> String
createIndex n = " %" ++ show n

toMessage' :: Int -> [Argument] ->  String
toMessage' _  []                            = ""
toMessage' n ((BasicArg _ _ p s):xs)        = p ++ createIndex n ++" "++ s ++ " " ++ toMessage'  (n+1) xs
toMessage' n (((TupleArg name p s x y)):xs) = p ++  s1 ++ " " ++ s ++  toMessage'  n1 xs
                                                where
                                                    (s1,n1) = toMessageHideName n (TupleArg name p s x y)
toMessage' n (DummyArg:xs)                  = createIndex n ++ " " ++  toMessage' (n+1) xs
toMessage' n ((StatementArg _ p t):xs)      = p ++ createIndex n ++ " " ++  toMessage' (n+1) xs


toMessageHideName :: Int -> Argument -> (String, Int)
toMessageHideName n BasicArg {}             = (createIndex n,n+1)
toMessageHideName n (TupleArg _ _ _ x y)    = (" (" ++ s1 ++ ", " ++ s2 ++ ")",n2)
                                                where
                                                    (s1,n1) = toMessageHideName n  x
                                                    (s2,n2) = toMessageHideName n1 y
toMessageHideName n DummyArg                = (createIndex n,n+1)
toMessageHideName n StatementArg {}         = (createIndex n,n+1)


class ToJS a where
    toJS :: a -> (String,String)

instance ToJS Argument where
    toJS (BasicArg Str name _ _)             = (toStringJS name, "var " ++ name ++ " = '\"' +  block.getFieldValue('" ++ name ++ "') + '\"'; \n")
    toJS (BasicArg _ name _ _)             = (toStringJS name, "var " ++ name ++ " = block.getFieldValue('" ++ name ++ "'); \n")
    toJS (StatementArg name _ _)           = (toStringJS name, "var " ++ name ++ " = Blockly.Python.valueToCode(block,'" ++ name ++ "',Blockly.Python.ORDER_MEMBER); \n") -- deze check is voorlopig nog niet correct denk ik
    toJS (TupleArg _ _ _ x y)              = ("'('+" ++ n1 ++ "+ ',' +"  ++  n2 ++ "+')'", arg1 ++ arg2)
                                            where
                                                (n1, arg1) = toJS x
                                                (n2, arg2) = toJS y
    toJS DummyArg                          = ("","")

toStringJS :: String -> String
toStringJS n = "String(" ++ n ++ ")"


instance ToJS [HatchBlock] where
    toJS xs = ("",foldr (\a t-> t ++ snd (toJS a) ) "" xs )

instance ToJS HatchBlock where
    toJS (RegularBlock name args) = ("" ,
                                    "Blockly.Python['" ++ name ++ "'] = function(block) { \n" ++
                                        arguments ++
                                    "    var code = '" ++ name ++ "'" ++ argNames ++ " ;\n" ++
                                    "    return code;\n}\n")
                                    where
                                        (argNames, arguments) = foldr ((\(a1,b1) (a2,b2) -> ("+ ' ' + " ++ a1 ++ " " ++ a2, "    " ++ b1 ++ b2)) . toJS) ("","") args

    toJS (ListBlock targetBlock name) = ([],
                                        "Blockly.Python['" ++ name ++ "'] = function(block) { \n" ++
                                        "    var define_blocks = block.getInputTargetBlock('" ++ targetBlock ++ "'); \n" ++
                                        "    var code = '[' ;\n" ++
                                        "    if (define_blocks){ \n" ++
                                        "        code += Blockly.Python.blockToCode(define_blocks,true) \n" ++
                                        "        while (define_blocks = define_blocks.getNextBlock()) { \n" ++
                                        "            code += ', ' + Blockly.Python.blockToCode(define_blocks,true); \n" ++
                                        "        } \n" ++
                                        "    } \n" ++
                                        "    code += ']'; \n" ++
                                        "    return [code, Blockly.JavaScript.ORDER_MEMBER ];\n}\n"
                                        )

    toJS (BasicBlock n a)             = ("",
                                        "Blockly.Python['" ++ n ++ "'] = function(block) { \n" ++
                                            arg  ++
                                        "   return " ++ name ++ ";\n}\n" 
                                        )
                                        where 
                                            (name, arg) = toJS a 


