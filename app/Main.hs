{-# LANGUAGE OverloadedStrings #-}
module Main where



import Hatch
import Text.Read
import Data.Text
main :: IO ()
main =
    startApp defineCategories [] config

config :: SiteConfig MeasurementElement
config = SiteConfig readMeasurementElement printMeasurementElement True


readMeasurementElement :: String -> Maybe Prog
readMeasurementElement = readMaybe


printMeasurementElement :: Prog -> IO ()
printMeasurementElement = print


type Prog = [MeasurementElement]

type LSIlluminationDuration = Int
type IrradiationParams = Text
type RobotName = Text
type RobotProgramName = Text
type DetectionParams = Text
type StageName = Text
type PositionNameAndCoords = (Text, (Double,Double))
type RelativeStageLoopParams = [Text]

data MeasurementElement =     MEDetection ![Text]
                            | MEIrradiation !LSIlluminationDuration ![IrradiationParams]
                            | MEWait !Double
                            | MEExecuteRobotProgram !RobotName !RobotProgramName !Bool
                            | MEDoTimes !Int !Prog
                            | MEFastAcquisitionLoop !Int !(Text, DetectionParams)
                            | METimeLapse !Int !Double !Prog
                            | MEStageLoop !StageName ![PositionNameAndCoords] !Prog
                            | MERelativeStageLoop !StageName !RelativeStageLoopParams !Prog
                        deriving (Show, Read)


instance Block MeasurementElement where
    toBlocks xs                      = ListBlock "Measurement Element" "Program"
    toBlock MEDetection {}           = RegularBlock "MEDetection" [StatementArg "Param" "Input text:" "Text"]
    toBlock MEIrradiation {}         = RegularBlock "MEIrradiation" [BasicArg Int "LSDur" "LS Ill Dur:" "", StatementArg "IrrParam" "Irr Params:" "Text"]
    toBlock MEWait {}                = RegularBlock "MEWait"  [BasicArg Float "time" "wait" "s"]
    toBlock MEExecuteRobotProgram {} = RegularBlock "MEExecuteRobotProgram"  [BasicArg Str "RobotName" "Name:" "", BasicArg Str "RobotProgramName" "Prog name:" "", BasicArg Bool "bool" "run?" "" ]
    toBlock MEDoTimes {}             = RegularBlock "MEDoTimes"  [BasicArg Int "times" "Do times:" "", StatementArg "prog" "Program input:" "Program"]
    toBlock MEFastAcquisitionLoop {} = RegularBlock "MEFastAcquisitionLoop"  [BasicArg Int "times" "Loop:" "", TupleArg "Detection" "" "" (BasicArg Str "text" "" "") (BasicArg Str "params" "" "") ]
    toBlock METimeLapse {}           = RegularBlock "METimeLapse" [BasicArg Int "interval" "framerate: " "",BasicArg Float "times" "Do times:" "", StatementArg "prog" "Program input: " "Program"]
    toBlock MEStageLoop {}           = RegularBlock "MEStageLoop" [BasicArg Str "stageName" "Stage Name:" "", StatementArg "posNameCords" "Positions:" "Position Name and Coords",StatementArg "prog" "Program input: " "Program"]
    toBlock MERelativeStageLoop {}   = RegularBlock "MeRelatveLoop" [BasicArg Str "stageName" "Stage name: " "", StatementArg "rslp" "Relative stage loop param:" "Text", StatementArg "prog" "Program input: " "Program"]


defineCategories :: [Category]
defineCategories = generateCategories "Measurement Element" 100 [
                    MEDetection [],
                    MEIrradiation 1 [],
                    MEWait 1,
                    MEExecuteRobotProgram "x" "x" True,
                    MEDoTimes 1 [],
                    MEFastAcquisitionLoop 1 ("",""),
                    METimeLapse 0 0 [],
                    MEStageLoop "" [] [],
                    MERelativeStageLoop "" [] []
                ] ++ [
                Category "Text" 20 [
                        ListBlock "Text" "Text",
                        BasicBlock "TextBlock" (BasicArg Str "input" "Text:" "")
                    ],
                Category "Position Name and Coords" 20 [
                        ListBlock "Position Name and Coords" "Position Name and Coords",
                        BasicBlock "rslp" (TupleArg "out" "Name, Coord:" "" (BasicArg Str "name" "" "") (TupleArg "tupleArg2" "" "" (BasicArg Float "x" "" "")  (BasicArg Float "y" "" "") ))
                    ]            
                ] 