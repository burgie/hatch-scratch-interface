# Hatch

## How to use

To install:
```
cabal update
stack build 
```
To run:
```
stack run
```


## Todo
- Finish all blocks
- Save to/load from File
- Show what went wrong if there is parse error
- (opt) generate dropdown list 
    * RobotName
    * RobotProgName